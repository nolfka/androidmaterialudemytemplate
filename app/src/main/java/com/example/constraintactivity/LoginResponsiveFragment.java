package com.example.constraintactivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class LoginResponsiveFragment extends Fragment implements View.OnFocusChangeListener {

    AppCompatEditText user;
    AppCompatEditText pass;

    TextInputLayout userLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_responsive_fragment, container, false);

        user = view.findViewById(R.id.username_textField);
        userLayout = view.findViewById(R.id.username_textInputLayout);

        pass = view.findViewById(R.id.password_textField);

/*        user.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                System.out.println("test onTextChanged " + charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/


        if (user != null) {
            System.out.println("1. -------------");
            user.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    System.out.println("ddddddddddddddddddddddd" + b);
                    if (user.getText().toString().isEmpty()) {
                        userLayout.setErrorEnabled(true);
                        userLayout.setError("Please enter your username");
                    } else {
                        userLayout.setErrorEnabled(false);
                    }
                }
            });
        }
        return view;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        System.out.println("jeeeeeeeeeeeeeeeeee");
    }
}
