package com.example.constraintactivity;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    AppCompatEditText user;
    TextInputLayout userLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, new MenuFragment())
                    .commit();
        }


        user = findViewById(R.id.username_textField);
        userLayout = findViewById(R.id.username_textInputLayout);
        user.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                System.out.println("ddddddddddddddddddddddd" + b);
                if(user.getText().toString().isEmpty()) {
                    userLayout.setErrorEnabled(true);
                    userLayout.setError("Please enter your username");
                } else {
                    userLayout.setErrorEnabled(false);
                }
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu); //add menu with items to toolbar
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.item) {
            System.out.println("login item selected....");
            navigateTo(new LoginResponsiveFragment(), false);
        } else if (id == R.id.item2) {
            navigateTo(new TextFieldFragment(), false);
        } else if (id == R.id.item3) {
            System.out.println("navigate to card view...Activity....");
            Intent cardViewIntent = new Intent(getApplicationContext(), CardViewActivity.class);
            startActivity(cardViewIntent);
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Navigate to the given fragment.
     *
     * @param fragment       Fragment to navigate to.
     * @param addToBackstack Whether or not the current fragment should be added to the backstack.
     */
    public void navigateTo(Fragment fragment, boolean addToBackstack) {
        FragmentTransaction transaction =
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, fragment).addToBackStack(null);

        if (addToBackstack) {
            transaction.addToBackStack(null);
        }


        transaction.commit();
    }
}
